## Data from planers performance measurements
# Suitable for gnuplot

#Problem	FF(time)	FF(steps)	LAMA(time)	LAMA(step)
20			2.53		149			31			162
25			3.72		147			45			165
30			4.28		149			96			161
35			10			147			82			167
40			16			147			111			167

## LAMA was used with all options (lLfF)
