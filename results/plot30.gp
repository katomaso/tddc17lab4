set terminal postscript landscape enhanced color dashed lw 2 "Helvetica" 13

set output "plot30-time.ps"
set autoscale

set xlabel "Number of people"
set ylabel "Approximate time [s]"

set style line 1 lt 1 lw 3 pt 3 lc rgb "red"
set style line 2 lt 3 lw 3 pt 3 lc rgb "red"
set style line 3 lt 1 lw 3 pt 3 lc rgb "blue"
set style line 4 lt 3 lw 3 pt 3 lc rgb "blue"

set yrange [0:130]
plot "results_30.gnuplot" using 1:2 title 'FF time' w lines ls 1, \
     "results_30.gnuplot" using 1:4 title 'LAMA time' w lines ls 3


set output "plot30-correct.ps"
set ylabel "Approximate correctness"

set yrange [0.5:1.1]
plot "results_30.gnuplot" using 1:(147/$3) title 'FF correctness' w lines ls 1, \
     "results_30.gnuplot" using 1:(148/$5) title 'LAMA correctness' w lines ls 3
