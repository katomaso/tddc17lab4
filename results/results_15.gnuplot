## Data from planers performance measurements
# Suitable for gnuplot

#Problem	FF(time)	FF(steps)	LAMA(time)	LAMA(step)
20			0.45		72			3.81		80
25			0.66		74			2.75		81
30			0.92		77			2.81		82
35			1.15		72			4.69		83
40			1.61		74			4.83		84

## LAMA was used with all options (lLfF)
