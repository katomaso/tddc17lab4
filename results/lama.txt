FF
===

20-15
-----
72
0.45

20-30
-----
149
2.53

25-15
-----
74
0.66

25-30
-----
147
3.72

30-15
-----
72
0.92

30-30
-----
149
4.28

35-15
-----
72
1.15

35-30
-----
147
10.2

40-15
-----
74
1.61

40-30
-----
147
15.56

LAMA (all algorithms)
=====================

20-15
-----
Plan length: 80 step(s).
Search time: 3.81 seconds

20-30
-----
Plan length: 162 step(s).
Search time: 30.76 seconds

25-15
-----
Plan length: 81 step(s).
Search time: 2.75 seconds

25-30
-----
Plan length: 165 step(s).
Search time: 45.15 seconds

30-15
-----
Plan length: 82 step(s).
Search time: 2.81 seconds

30-30
-----
Plan length: 161 step(s).
Search time: 96.44 seconds

35-15
-----
Plan length: 83 step(s).
Search time: 4.69 seconds

35-30
-----
Plan length: 167 step(s).
Search time: 81.94 seconds

40-15
-----
Plan length: 84 step(s).
Search time: 4.83 seconds

40-30
-----
Plan length: 167 step(s).
Search time: 118.37 seconds


LAMA (ff algorithm)
=====================

20-15
-----
Plan length: 79 step(s).
Expanded 23262 state(s).
Generated 957131 state(s).
Search time: 51.85 seconds

20-30
-----
Plan length: 171 step(s).
Expanded 97032 state(s).
Generated 4032982 state(s).
Search time: 380.2 seconds

LAMA (landmark algorithm)
=========================

20-15
-----
Plan length: 73 step(s).
Expanded 128252 state(s).
Generated 5320483 state(s).
Search time: 188.87 seconds


25-15
-----
Plan length: 73 step(s).
Expanded 189371 state(s).
Generated 9727648 state(s).
Search time: 301.07 seconds

20-30
-----
Plan length: 148 step(s).
Expanded 481586 state(s).
Generated 20194577 state(s).
Search time: 902.04 seconds
Total time: 903.13 seconds

