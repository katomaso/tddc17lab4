set terminal postscript landscape enhanced color dashed lw 2 "Helvetica" 13

set output "plot_crates-time.ps"

set xlabel "Number of crates"
set ylabel "Approximate time [s]"

set style line 1 lt 1 lw 3 pt 3 lc rgb "red"
set style line 2 lt 3 lw 3 pt 3 lc rgb "red"
set style line 3 lt 1 lw 3 pt 3 lc rgb "blue"
set style line 4 lt 3 lw 3 pt 3 lc rgb "blue"

set yrange [1:512]
set logscale y 2
plot "../tests/result.txt" using 1:5 title 'FF time' w lines ls 1, \
     "../tests/result.txt" using 1:3 title 'LAMA time' w lines ls 3


set output "plot_crates-correct.ps"
set ylabel "Approximate correctness"

set yrange [0.5:1.1]
unset logscale y

plot "../tests/result.txt" using 1:(((5*$1)-3)/$4) title 'FF correctness' w lines ls 1, \
     "../tests/result.txt" using 1:(((5*$1)-2)/$2) title 'LAMA correctness' w lines ls 3

