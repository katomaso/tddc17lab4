
(define (problem emergency-30-26)
    (:domain EMERGENCY)

    (:objects
        DEPOT UAV1 UAV2
        
            FOOD0 FOOD1 FOOD2 FOOD3 FOOD4 FOOD5 FOOD6 FOOD7 FOOD8 FOOD9 FOOD10 FOOD11 FOOD12 FOOD13 FOOD14 
        
            WATER0 WATER1 WATER2 WATER3 WATER4 WATER5 WATER6 WATER7 
        
            DRUGS0 DRUGS1 DRUGS2 
        
            PERSON0 PERSON1 PERSON2 PERSON3 PERSON4 PERSON5 PERSON6 PERSON7 PERSON8 PERSON9 PERSON10 PERSON11 PERSON12 PERSON13 PERSON14 PERSON15 PERSON16 PERSON17 PERSON18 PERSON19 PERSON20 PERSON21 PERSON22 PERSON23 PERSON24 PERSON25 PERSON26 PERSON27 PERSON28 PERSON29 
        
    )

    (:init
        (location DEPOT) (depot DEPOT)
        (helicopter UAV1) (helicopter UAV2) (empty UAV1) (empty UAV2) (at UAV1 DEPOT) (at UAV2 DEPOT)

        (location PERSON0) (person PERSON0)
        (location PERSON1) (person PERSON1)
        (location PERSON2) (person PERSON2)
        (location PERSON3) (person PERSON3)
        (location PERSON4) (person PERSON4)
        (location PERSON5) (person PERSON5)
        (location PERSON6) (person PERSON6)
        (location PERSON7) (person PERSON7)
        (location PERSON8) (person PERSON8)
        (location PERSON9) (person PERSON9)
        (location PERSON10) (person PERSON10)
        (location PERSON11) (person PERSON11)
        (location PERSON12) (person PERSON12)
        (location PERSON13) (person PERSON13)
        (location PERSON14) (person PERSON14)
        (location PERSON15) (person PERSON15)
        (location PERSON16) (person PERSON16)
        (location PERSON17) (person PERSON17)
        (location PERSON18) (person PERSON18)
        (location PERSON19) (person PERSON19)
        (location PERSON20) (person PERSON20)
        (location PERSON21) (person PERSON21)
        (location PERSON22) (person PERSON22)
        (location PERSON23) (person PERSON23)
        (location PERSON24) (person PERSON24)
        (location PERSON25) (person PERSON25)
        (location PERSON26) (person PERSON26)
        (location PERSON27) (person PERSON27)
        (location PERSON28) (person PERSON28)
        (location PERSON29) (person PERSON29)
        

        (crate FOOD0) (food_crate FOOD0) (at FOOD0 DEPOT)
        (crate FOOD1) (food_crate FOOD1) (at FOOD1 DEPOT)
        (crate FOOD2) (food_crate FOOD2) (at FOOD2 DEPOT)
        (crate FOOD3) (food_crate FOOD3) (at FOOD3 DEPOT)
        (crate FOOD4) (food_crate FOOD4) (at FOOD4 DEPOT)
        (crate FOOD5) (food_crate FOOD5) (at FOOD5 DEPOT)
        (crate FOOD6) (food_crate FOOD6) (at FOOD6 DEPOT)
        (crate FOOD7) (food_crate FOOD7) (at FOOD7 DEPOT)
        (crate FOOD8) (food_crate FOOD8) (at FOOD8 DEPOT)
        (crate FOOD9) (food_crate FOOD9) (at FOOD9 DEPOT)
        (crate FOOD10) (food_crate FOOD10) (at FOOD10 DEPOT)
        (crate FOOD11) (food_crate FOOD11) (at FOOD11 DEPOT)
        (crate FOOD12) (food_crate FOOD12) (at FOOD12 DEPOT)
        (crate FOOD13) (food_crate FOOD13) (at FOOD13 DEPOT)
        (crate FOOD14) (food_crate FOOD14) (at FOOD14 DEPOT)
        

        (crate WATER0) (water_crate WATER0) (at WATER0 DEPOT)
        (crate WATER1) (water_crate WATER1) (at WATER1 DEPOT)
        (crate WATER2) (water_crate WATER2) (at WATER2 DEPOT)
        (crate WATER3) (water_crate WATER3) (at WATER3 DEPOT)
        (crate WATER4) (water_crate WATER4) (at WATER4 DEPOT)
        (crate WATER5) (water_crate WATER5) (at WATER5 DEPOT)
        (crate WATER6) (water_crate WATER6) (at WATER6 DEPOT)
        (crate WATER7) (water_crate WATER7) (at WATER7 DEPOT)
        

        (crate DRUGS0) (drugs_crate DRUGS0) (at DRUGS0 DEPOT)
        (crate DRUGS1) (drugs_crate DRUGS1) (at DRUGS1 DEPOT)
        (crate DRUGS2) (drugs_crate DRUGS2) (at DRUGS2 DEPOT)
        

        (hungry PERSON20) (hungry PERSON2) (hungry PERSON8) (hungry PERSON7) (hungry PERSON16) (hungry PERSON12) (hungry PERSON5) (hungry PERSON22) (hungry PERSON10) (hungry PERSON14) (hungry PERSON28) (hungry PERSON26) (hungry PERSON23) (hungry PERSON6) (hungry PERSON1) 
        (thirsty PERSON11) (thirsty PERSON15) (thirsty PERSON0) (thirsty PERSON1) (thirsty PERSON25) (thirsty PERSON14) (thirsty PERSON2) (thirsty PERSON17) 
        (injured PERSON11) (injured PERSON7) (injured PERSON18) 
    )

    (:goal
        (and
            (not (hungry PERSON20)) (not (hungry PERSON2)) (not (hungry PERSON8)) (not (hungry PERSON7)) (not (hungry PERSON16)) (not (hungry PERSON12)) (not (hungry PERSON5)) (not (hungry PERSON22)) (not (hungry PERSON10)) (not (hungry PERSON14)) (not (hungry PERSON28)) (not (hungry PERSON26)) (not (hungry PERSON23)) (not (hungry PERSON6)) (not (hungry PERSON1)) 
            (not (thirsty PERSON11)) (not (thirsty PERSON15)) (not (thirsty PERSON0)) (not (thirsty PERSON1)) (not (thirsty PERSON25)) (not (thirsty PERSON14)) (not (thirsty PERSON2)) (not (thirsty PERSON17)) 
            (not (injured PERSON11)) (not (injured PERSON7)) (not (injured PERSON18)) 
        )
    )
)