#!/usr/bin/env python

from subprocess import call
import random
import itertools

for i in range(20, 42, 2):
    args = [30, ]
    args.append(random.randint(1,i))
    args.append(random.randint(0,i-args[1]))
    args.append(i-(args[2]+args[1]))
    call(itertools.chain(['../problem_generator.py',], map(str, args)))

