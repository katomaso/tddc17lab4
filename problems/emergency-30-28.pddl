
(define (problem emergency-30-28)
    (:domain EMERGENCY)

    (:objects
        DEPOT UAV1 UAV2
        
            FOOD0 
        
            WATER0 WATER1 WATER2 WATER3 WATER4 WATER5 WATER6 WATER7 WATER8 WATER9 WATER10 
        
            DRUGS0 DRUGS1 DRUGS2 DRUGS3 DRUGS4 DRUGS5 DRUGS6 DRUGS7 DRUGS8 DRUGS9 DRUGS10 DRUGS11 DRUGS12 DRUGS13 DRUGS14 DRUGS15 
        
            PERSON0 PERSON1 PERSON2 PERSON3 PERSON4 PERSON5 PERSON6 PERSON7 PERSON8 PERSON9 PERSON10 PERSON11 PERSON12 PERSON13 PERSON14 PERSON15 PERSON16 PERSON17 PERSON18 PERSON19 PERSON20 PERSON21 PERSON22 PERSON23 PERSON24 PERSON25 PERSON26 PERSON27 PERSON28 PERSON29 
        
    )

    (:init
        (location DEPOT) (depot DEPOT)
        (helicopter UAV1) (helicopter UAV2) (empty UAV1) (empty UAV2) (at UAV1 DEPOT) (at UAV2 DEPOT)

        (location PERSON0) (person PERSON0)
        (location PERSON1) (person PERSON1)
        (location PERSON2) (person PERSON2)
        (location PERSON3) (person PERSON3)
        (location PERSON4) (person PERSON4)
        (location PERSON5) (person PERSON5)
        (location PERSON6) (person PERSON6)
        (location PERSON7) (person PERSON7)
        (location PERSON8) (person PERSON8)
        (location PERSON9) (person PERSON9)
        (location PERSON10) (person PERSON10)
        (location PERSON11) (person PERSON11)
        (location PERSON12) (person PERSON12)
        (location PERSON13) (person PERSON13)
        (location PERSON14) (person PERSON14)
        (location PERSON15) (person PERSON15)
        (location PERSON16) (person PERSON16)
        (location PERSON17) (person PERSON17)
        (location PERSON18) (person PERSON18)
        (location PERSON19) (person PERSON19)
        (location PERSON20) (person PERSON20)
        (location PERSON21) (person PERSON21)
        (location PERSON22) (person PERSON22)
        (location PERSON23) (person PERSON23)
        (location PERSON24) (person PERSON24)
        (location PERSON25) (person PERSON25)
        (location PERSON26) (person PERSON26)
        (location PERSON27) (person PERSON27)
        (location PERSON28) (person PERSON28)
        (location PERSON29) (person PERSON29)
        

        (crate FOOD0) (food_crate FOOD0) (at FOOD0 DEPOT)
        

        (crate WATER0) (water_crate WATER0) (at WATER0 DEPOT)
        (crate WATER1) (water_crate WATER1) (at WATER1 DEPOT)
        (crate WATER2) (water_crate WATER2) (at WATER2 DEPOT)
        (crate WATER3) (water_crate WATER3) (at WATER3 DEPOT)
        (crate WATER4) (water_crate WATER4) (at WATER4 DEPOT)
        (crate WATER5) (water_crate WATER5) (at WATER5 DEPOT)
        (crate WATER6) (water_crate WATER6) (at WATER6 DEPOT)
        (crate WATER7) (water_crate WATER7) (at WATER7 DEPOT)
        (crate WATER8) (water_crate WATER8) (at WATER8 DEPOT)
        (crate WATER9) (water_crate WATER9) (at WATER9 DEPOT)
        (crate WATER10) (water_crate WATER10) (at WATER10 DEPOT)
        

        (crate DRUGS0) (drugs_crate DRUGS0) (at DRUGS0 DEPOT)
        (crate DRUGS1) (drugs_crate DRUGS1) (at DRUGS1 DEPOT)
        (crate DRUGS2) (drugs_crate DRUGS2) (at DRUGS2 DEPOT)
        (crate DRUGS3) (drugs_crate DRUGS3) (at DRUGS3 DEPOT)
        (crate DRUGS4) (drugs_crate DRUGS4) (at DRUGS4 DEPOT)
        (crate DRUGS5) (drugs_crate DRUGS5) (at DRUGS5 DEPOT)
        (crate DRUGS6) (drugs_crate DRUGS6) (at DRUGS6 DEPOT)
        (crate DRUGS7) (drugs_crate DRUGS7) (at DRUGS7 DEPOT)
        (crate DRUGS8) (drugs_crate DRUGS8) (at DRUGS8 DEPOT)
        (crate DRUGS9) (drugs_crate DRUGS9) (at DRUGS9 DEPOT)
        (crate DRUGS10) (drugs_crate DRUGS10) (at DRUGS10 DEPOT)
        (crate DRUGS11) (drugs_crate DRUGS11) (at DRUGS11 DEPOT)
        (crate DRUGS12) (drugs_crate DRUGS12) (at DRUGS12 DEPOT)
        (crate DRUGS13) (drugs_crate DRUGS13) (at DRUGS13 DEPOT)
        (crate DRUGS14) (drugs_crate DRUGS14) (at DRUGS14 DEPOT)
        (crate DRUGS15) (drugs_crate DRUGS15) (at DRUGS15 DEPOT)
        

        (hungry PERSON21) 
        (thirsty PERSON20) (thirsty PERSON19) (thirsty PERSON5) (thirsty PERSON26) (thirsty PERSON1) (thirsty PERSON23) (thirsty PERSON8) (thirsty PERSON14) (thirsty PERSON29) (thirsty PERSON12) (thirsty PERSON4) 
        (injured PERSON13) (injured PERSON3) (injured PERSON14) (injured PERSON6) (injured PERSON18) (injured PERSON25) (injured PERSON1) (injured PERSON10) (injured PERSON26) (injured PERSON4) (injured PERSON15) (injured PERSON5) (injured PERSON11) (injured PERSON24) (injured PERSON19) (injured PERSON20) 
    )

    (:goal
        (and
            (not (hungry PERSON21)) 
            (not (thirsty PERSON20)) (not (thirsty PERSON19)) (not (thirsty PERSON5)) (not (thirsty PERSON26)) (not (thirsty PERSON1)) (not (thirsty PERSON23)) (not (thirsty PERSON8)) (not (thirsty PERSON14)) (not (thirsty PERSON29)) (not (thirsty PERSON12)) (not (thirsty PERSON4)) 
            (not (injured PERSON13)) (not (injured PERSON3)) (not (injured PERSON14)) (not (injured PERSON6)) (not (injured PERSON18)) (not (injured PERSON25)) (not (injured PERSON1)) (not (injured PERSON10)) (not (injured PERSON26)) (not (injured PERSON4)) (not (injured PERSON15)) (not (injured PERSON5)) (not (injured PERSON11)) (not (injured PERSON24)) (not (injured PERSON19)) (not (injured PERSON20)) 
        )
    )
)