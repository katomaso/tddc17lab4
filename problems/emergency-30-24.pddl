
(define (problem emergency-30-24)
    (:domain EMERGENCY)

    (:objects
        DEPOT UAV1 UAV2
        
            FOOD0 FOOD1 FOOD2 FOOD3 FOOD4 FOOD5 FOOD6 FOOD7 FOOD8 FOOD9 FOOD10 FOOD11 FOOD12 FOOD13 FOOD14 FOOD15 FOOD16 FOOD17 FOOD18 FOOD19 FOOD20 FOOD21 FOOD22 
        
            
        
            DRUGS0 
        
            PERSON0 PERSON1 PERSON2 PERSON3 PERSON4 PERSON5 PERSON6 PERSON7 PERSON8 PERSON9 PERSON10 PERSON11 PERSON12 PERSON13 PERSON14 PERSON15 PERSON16 PERSON17 PERSON18 PERSON19 PERSON20 PERSON21 PERSON22 PERSON23 PERSON24 PERSON25 PERSON26 PERSON27 PERSON28 PERSON29 
        
    )

    (:init
        (location DEPOT) (depot DEPOT)
        (helicopter UAV1) (helicopter UAV2) (empty UAV1) (empty UAV2) (at UAV1 DEPOT) (at UAV2 DEPOT)

        (location PERSON0) (person PERSON0)
        (location PERSON1) (person PERSON1)
        (location PERSON2) (person PERSON2)
        (location PERSON3) (person PERSON3)
        (location PERSON4) (person PERSON4)
        (location PERSON5) (person PERSON5)
        (location PERSON6) (person PERSON6)
        (location PERSON7) (person PERSON7)
        (location PERSON8) (person PERSON8)
        (location PERSON9) (person PERSON9)
        (location PERSON10) (person PERSON10)
        (location PERSON11) (person PERSON11)
        (location PERSON12) (person PERSON12)
        (location PERSON13) (person PERSON13)
        (location PERSON14) (person PERSON14)
        (location PERSON15) (person PERSON15)
        (location PERSON16) (person PERSON16)
        (location PERSON17) (person PERSON17)
        (location PERSON18) (person PERSON18)
        (location PERSON19) (person PERSON19)
        (location PERSON20) (person PERSON20)
        (location PERSON21) (person PERSON21)
        (location PERSON22) (person PERSON22)
        (location PERSON23) (person PERSON23)
        (location PERSON24) (person PERSON24)
        (location PERSON25) (person PERSON25)
        (location PERSON26) (person PERSON26)
        (location PERSON27) (person PERSON27)
        (location PERSON28) (person PERSON28)
        (location PERSON29) (person PERSON29)
        

        (crate FOOD0) (food_crate FOOD0) (at FOOD0 DEPOT)
        (crate FOOD1) (food_crate FOOD1) (at FOOD1 DEPOT)
        (crate FOOD2) (food_crate FOOD2) (at FOOD2 DEPOT)
        (crate FOOD3) (food_crate FOOD3) (at FOOD3 DEPOT)
        (crate FOOD4) (food_crate FOOD4) (at FOOD4 DEPOT)
        (crate FOOD5) (food_crate FOOD5) (at FOOD5 DEPOT)
        (crate FOOD6) (food_crate FOOD6) (at FOOD6 DEPOT)
        (crate FOOD7) (food_crate FOOD7) (at FOOD7 DEPOT)
        (crate FOOD8) (food_crate FOOD8) (at FOOD8 DEPOT)
        (crate FOOD9) (food_crate FOOD9) (at FOOD9 DEPOT)
        (crate FOOD10) (food_crate FOOD10) (at FOOD10 DEPOT)
        (crate FOOD11) (food_crate FOOD11) (at FOOD11 DEPOT)
        (crate FOOD12) (food_crate FOOD12) (at FOOD12 DEPOT)
        (crate FOOD13) (food_crate FOOD13) (at FOOD13 DEPOT)
        (crate FOOD14) (food_crate FOOD14) (at FOOD14 DEPOT)
        (crate FOOD15) (food_crate FOOD15) (at FOOD15 DEPOT)
        (crate FOOD16) (food_crate FOOD16) (at FOOD16 DEPOT)
        (crate FOOD17) (food_crate FOOD17) (at FOOD17 DEPOT)
        (crate FOOD18) (food_crate FOOD18) (at FOOD18 DEPOT)
        (crate FOOD19) (food_crate FOOD19) (at FOOD19 DEPOT)
        (crate FOOD20) (food_crate FOOD20) (at FOOD20 DEPOT)
        (crate FOOD21) (food_crate FOOD21) (at FOOD21 DEPOT)
        (crate FOOD22) (food_crate FOOD22) (at FOOD22 DEPOT)
        

        

        (crate DRUGS0) (drugs_crate DRUGS0) (at DRUGS0 DEPOT)
        

        (hungry PERSON13) (hungry PERSON10) (hungry PERSON26) (hungry PERSON22) (hungry PERSON21) (hungry PERSON3) (hungry PERSON23) (hungry PERSON6) (hungry PERSON15) (hungry PERSON4) (hungry PERSON17) (hungry PERSON2) (hungry PERSON27) (hungry PERSON8) (hungry PERSON20) (hungry PERSON1) (hungry PERSON18) (hungry PERSON7) (hungry PERSON29) (hungry PERSON11) (hungry PERSON14) (hungry PERSON28) (hungry PERSON0) 
        
        (injured PERSON21) 
    )

    (:goal
        (and
            (not (hungry PERSON13)) (not (hungry PERSON10)) (not (hungry PERSON26)) (not (hungry PERSON22)) (not (hungry PERSON21)) (not (hungry PERSON3)) (not (hungry PERSON23)) (not (hungry PERSON6)) (not (hungry PERSON15)) (not (hungry PERSON4)) (not (hungry PERSON17)) (not (hungry PERSON2)) (not (hungry PERSON27)) (not (hungry PERSON8)) (not (hungry PERSON20)) (not (hungry PERSON1)) (not (hungry PERSON18)) (not (hungry PERSON7)) (not (hungry PERSON29)) (not (hungry PERSON11)) (not (hungry PERSON14)) (not (hungry PERSON28)) (not (hungry PERSON0)) 
            
            (not (injured PERSON21)) 
        )
    )
)