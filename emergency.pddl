(define (domain EMERGENCY)
	(:requirements :strips)
		
	(:predicates
		;; generic
		(at ?what ?where)
		(empty ?o)
		(location ?o)
		
		;; helicopter
		; using: at, empty
		(helicopter ?o)
		
		;; person
		; using: at
		(person ?o)
		(hungry ?who)
		(thirsty ?who)
		(injured ?who)
		
		;; depot
		(depot ?loc)
		
		;; crate
		(crate ?o)
        (food_crate ?crate)
        (water_crate ?crate)
        (drugs_crate ?crate)
	)

	(:action load
		:parameters (?uav ?crate ?loc)
		:precondition (and (helicopter ?uav) (crate ?crate) (depot ?loc) (empty ?uav) (at ?crate ?loc) (at ?uav ?loc))
		:effect (and (not (empty ?uav)) (at ?crate ?uav) (not (at ?crate ?loc)))
	)
	
	(:action fly
		:parameters (?uav ?from ?to)
		:precondition (and (helicopter ?uav) (location ?from) (location ?to) (at ?uav ?from))
		:effect (and (not(at ?uav ?from)) (at ?uav ?to))
	)
	
	(:action unload
		:parameters (?uav ?crate ?loc)
		:precondition (and (helicopter ?uav) (crate ?crate) (location ?loc) (at ?crate ?uav) (at ?uav ?loc))
		:effect (and (empty ?uav) (not (at ?crate ?uav)) (at ?crate ?loc))
	)
	
	(:action eat
	   :parameters (?p ?crate)
	   :precondition (and (person ?p) (hungry ?p) (food_crate ?crate) (at ?crate ?p))
	   :effect (and (not (hungry ?p)) (not (at ?crate ?p)))
	)
	
	(:action drink
	   :parameters (?p ?crate)
	   :precondition (and (person ?p) (thirsty ?p) (water_crate ?crate) (at ?crate ?p))
	   :effect (and (not (thirsty ?p)) (not (at ?crate ?p)))
	)
	
	(:action cure
	   :parameters (?p ?crate)
	   :precondition (and (person ?p) (injured ?p) (drugs_crate ?crate) (at ?crate ?p))
	   :effect (and (not (injured ?p)) (not (at ?crate ?p)))
	)

)
