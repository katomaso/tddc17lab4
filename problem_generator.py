#!/usr/bin/env python3
# coding: utf-8
'''
This script is supplementary material for tompe625 and phikl641
for subject TDDC17 Artificial Intelligence at Linköping University

It generates problems for domain EMERGENCY in PDDL.

Script does not require any arguments.
Uses python3 and python-jinja2 template engine.

Made by Tomas Peterka
'''

## user setting area

persons = 40

foods = 37
waters = 14
drugs = 27

## end of user setting area

import io
import random
import jinja2
import sys

if len(sys.argv) == 5:
    persons = int(sys.argv[1])
    foods = int(sys.argv[2])
    waters = int(sys.argv[4])
    drugs = int(sys.argv[3])

version = "{0}-{1}".format(persons, foods+drugs+waters)

objects = {
    "food": ["FOOD%d" % i for i in range(foods)],
    "water": ["WATER%d" % i  for i in range(waters)],
    "drugs": ["DRUGS%d" % i  for i in range(drugs)],
    "person": ["PERSON%d" % i  for i in range(persons)],
}

sufferings = {
    "hungry": ["PERSON%d" % i for i in random.sample(range(persons), min(persons,foods))],
    "thirsty": ["PERSON%d" % i for i in random.sample(range(persons), min(persons,waters))],
    "injured": ["PERSON%d" % i for i in random.sample(range(persons), min(persons,drugs))],
}

template = jinja2.Template("""
(define (problem emergency-{{ version }})
    (:domain EMERGENCY)

    (:objects
        DEPOT UAV1 UAV2
        {% for objs in objects.values() %}
            {% for obj in objs %}{{ obj }} {% endfor %}
        {% endfor %}
    )

    (:init
        (location DEPOT) (depot DEPOT)
        (helicopter UAV1) (helicopter UAV2) (empty UAV1) (empty UAV2) (at UAV1 DEPOT) (at UAV2 DEPOT)

        {% for person in objects.person %}(location {{ person }}) (person {{ person }})
        {% endfor %}

        {% for food in objects.food %}(crate {{ food }}) (food_crate {{ food }}) (at {{ food }} DEPOT)
        {% endfor %}

        {% for water in objects.water %}(crate {{ water }}) (water_crate {{ water }}) (at {{ water }} DEPOT)
        {% endfor %}

        {% for drugs in objects.drugs %}(crate {{ drugs }}) (drugs_crate {{ drugs }}) (at {{ drugs }} DEPOT)
        {% endfor %}

        {% for person in sufferings.hungry %}(hungry {{ person }}) {% endfor %}
        {% for person in sufferings.thirsty %}(thirsty {{ person }}) {% endfor %}
        {% for person in sufferings.injured %}(injured {{ person }}) {% endfor %}
    )

    (:goal
        (and
            {% for person in sufferings.hungry %}(not (hungry {{ person }})) {% endfor %}
            {% for person in sufferings.thirsty %}(not (thirsty {{ person }})) {% endfor %}
            {% for person in sufferings.injured %}(not (injured {{ person }})) {% endfor %}
        )
    )
)
""")

filename = "emergency-{0}.pddl".format(version)
#~ choice = input("write to file? (y/n) ")

output = template.render(objects=objects, sufferings=sufferings, version=version)

#~ if choice != "y":
    #~ print(output)
#~
#~ else:
with open(filename, "w") as ostream:
    ostream.write(output)
print("Output written to {0}".format(filename))

