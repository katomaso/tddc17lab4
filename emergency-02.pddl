(define (problem emergency-01)
    (:domain EMERGENCY)
    
    (:objects 
        UAV1 UAV2
        P1 P2 P3
        FOOD1 FOOD2 FOOD3
        DRUGS1
        WATER1 WATER2
        DEPOT
    )
    
    (:init
        (location DEPOT) (depot DEPOT)
        (location P1) (person P1) (hungry P1)
        (location P2) (person P2) (hungry P2) (thirsty P2) (injured P2)
        (location P3) (person P3) (hungry P3)
        
        (helicopter UAV1) (helicopter UAV2) (empty UAV1) (empty UAV2)
        (crate FOOD1) (food_crate FOOD1) (crate FOOD2) (food_crate FOOD2) (crate FOOD3) (food_crate FOOD3)
        (crate DRUGS1) (drugs_crate DRUGS1)
        (crate WATER1) (water_crate WATER1) (crate WATER2) (water_crate WATER2)
        (at UAV1 DEPOT) (at UAV2 DEPOT) (at FOOD1 DEPOT) (at FOOD2 DEPOT) (at FOOD3 DEPOT)
        (at WATER1 DEPOT) (at WATER2 DEPOT) (at DRUGS1 DEPOT)
    )
    
    (:goal
        (and 
            (not (hungry P1)) (not (thirsty P1)) (not (injured P1))
            (not (hungry P2)) (not (thirsty P2)) (not (injured P2))
            (not (hungry P3)) (not (thirsty P3)) (not (injured P3))
        )
    )
)
