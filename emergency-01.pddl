(define (problem emergency-01)
    (:domain EMERGENCY)
    
    (:objects 
        UAV1 UAV2
        P1
        FOOD1
        DEPOT
    )
    
    (:init
        (location DEPOT) (depot DEPOT)
        (location P1) (person P1) (hungry P1)
        (helicopter UAV1) (helicopter UAV2) (empty UAV1) (empty UAV2)
        (crate FOOD1) (food_crate FOOD1)
        (at UAV1 DEPOT) (at UAV2 DEPOT) (at FOOD1 DEPOT)
    )
    
    (:goal
        (and 
            (not (hungry P1))
        )
    )
)
