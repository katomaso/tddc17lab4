
(define (problem emergency-20-15)
    (:domain EMERGENCY)

    (:objects
        DEPOT UAV1 UAV2
        
            FOOD0 FOOD1 FOOD2 FOOD3 FOOD4 
        
            WATER0 WATER1 WATER2 WATER3 WATER4 
        
            DRUGS0 DRUGS1 DRUGS2 DRUGS3 DRUGS4 
        
            PERSON0 PERSON1 PERSON2 PERSON3 PERSON4 PERSON5 PERSON6 PERSON7 PERSON8 PERSON9 PERSON10 PERSON11 PERSON12 PERSON13 PERSON14 PERSON15 PERSON16 PERSON17 PERSON18 PERSON19 
        
    )

    (:init
        (location DEPOT) (depot DEPOT)
        (helicopter UAV1) (helicopter UAV2) (empty UAV1) (empty UAV2) (at UAV1 DEPOT) (at UAV2 DEPOT)

        (location PERSON0) (person PERSON0)
        (location PERSON1) (person PERSON1)
        (location PERSON2) (person PERSON2)
        (location PERSON3) (person PERSON3)
        (location PERSON4) (person PERSON4)
        (location PERSON5) (person PERSON5)
        (location PERSON6) (person PERSON6)
        (location PERSON7) (person PERSON7)
        (location PERSON8) (person PERSON8)
        (location PERSON9) (person PERSON9)
        (location PERSON10) (person PERSON10)
        (location PERSON11) (person PERSON11)
        (location PERSON12) (person PERSON12)
        (location PERSON13) (person PERSON13)
        (location PERSON14) (person PERSON14)
        (location PERSON15) (person PERSON15)
        (location PERSON16) (person PERSON16)
        (location PERSON17) (person PERSON17)
        (location PERSON18) (person PERSON18)
        (location PERSON19) (person PERSON19)
        

        (crate FOOD0) (food_crate FOOD0) (at FOOD0 DEPOT)
        (crate FOOD1) (food_crate FOOD1) (at FOOD1 DEPOT)
        (crate FOOD2) (food_crate FOOD2) (at FOOD2 DEPOT)
        (crate FOOD3) (food_crate FOOD3) (at FOOD3 DEPOT)
        (crate FOOD4) (food_crate FOOD4) (at FOOD4 DEPOT)
        

        (crate WATER0) (water_crate WATER0) (at WATER0 DEPOT)
        (crate WATER1) (water_crate WATER1) (at WATER1 DEPOT)
        (crate WATER2) (water_crate WATER2) (at WATER2 DEPOT)
        (crate WATER3) (water_crate WATER3) (at WATER3 DEPOT)
        (crate WATER4) (water_crate WATER4) (at WATER4 DEPOT)
        

        (crate DRUGS0) (drugs_crate DRUGS0) (at DRUGS0 DEPOT)
        (crate DRUGS1) (drugs_crate DRUGS1) (at DRUGS1 DEPOT)
        (crate DRUGS2) (drugs_crate DRUGS2) (at DRUGS2 DEPOT)
        (crate DRUGS3) (drugs_crate DRUGS3) (at DRUGS3 DEPOT)
        (crate DRUGS4) (drugs_crate DRUGS4) (at DRUGS4 DEPOT)
        

        (hungry PERSON18) (hungry PERSON15) (hungry PERSON10) (hungry PERSON5) (hungry PERSON16) 
        (thirsty PERSON5) (thirsty PERSON14) (thirsty PERSON10) (thirsty PERSON4) (thirsty PERSON17) 
        (injured PERSON17) (injured PERSON5) (injured PERSON15) (injured PERSON0) (injured PERSON9) 
    )

    (:goal
        (and
            (not (hungry PERSON18)) (not (hungry PERSON15)) (not (hungry PERSON10)) (not (hungry PERSON5)) (not (hungry PERSON16)) 
            (not (thirsty PERSON5)) (not (thirsty PERSON14)) (not (thirsty PERSON10)) (not (thirsty PERSON4)) (not (thirsty PERSON17)) 
            (not (injured PERSON17)) (not (injured PERSON5)) (not (injured PERSON15)) (not (injured PERSON0)) (not (injured PERSON9)) 
        )
    )
)