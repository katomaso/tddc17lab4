(define: (domain HAWAI)
    (:requirements :strips :adl)

    (:predicates
    	(wealth ?who)
    	(drunk ?who)
    	(sexy ?who)
    )
    
    (:action to_sun
        	:parameters (?who)
        	:effect(sexy ?who)
    )

    (:action to_party
        	:parameters(?who)
        	:precondition(wealth ?who)
        	:effect(drunk ?who)
    )
)
