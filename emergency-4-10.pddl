
(define (problem emergency-4-10)
    (:domain EMERGENCY)

    (:objects
        DEPOT UAV1 UAV2
        
            FOOD0 FOOD1 FOOD2 FOOD3 FOOD4 
        
            DRUGS0 DRUGS1 
        
            WATER0 WATER1 WATER2 
        
            PERSON0 PERSON1 PERSON2 PERSON3 
        
    )

    (:init
        (location DEPOT) (depot DEPOT)
        (helicopter UAV1) (helicopter UAV2) (empty UAV1) (empty UAV2) (at UAV1 DEPOT) (at UAV2 DEPOT)

        (location PERSON0) (person PERSON0)
        (location PERSON1) (person PERSON1)
        (location PERSON2) (person PERSON2)
        (location PERSON3) (person PERSON3)
        

        (crate FOOD0) (food_crate FOOD0) (at FOOD0 DEPOT)
        (crate FOOD1) (food_crate FOOD1) (at FOOD1 DEPOT)
        (crate FOOD2) (food_crate FOOD2) (at FOOD2 DEPOT)
        (crate FOOD3) (food_crate FOOD3) (at FOOD3 DEPOT)
        (crate FOOD4) (food_crate FOOD4) (at FOOD4 DEPOT)
        

        (crate WATER0) (water_crate WATER0) (at WATER0 DEPOT)
        (crate WATER1) (water_crate WATER1) (at WATER1 DEPOT)
        (crate WATER2) (water_crate WATER2) (at WATER2 DEPOT)
        

        (crate DRUGS0) (drugs_crate DRUGS0) (at DRUGS0 DEPOT)
        (crate DRUGS1) (drugs_crate DRUGS1) (at DRUGS1 DEPOT)
        

        (hungry PERSON1) (hungry PERSON3) (hungry PERSON2) (hungry PERSON0) 
        (thirsty PERSON1) (thirsty PERSON0) (thirsty PERSON2) 
        (injured PERSON0) (injured PERSON1) 
    )

    (:goal
        (and
            (not (hungry PERSON0)) (not (thirsty PERSON0)) (not (injured PERSON0))
            (not (hungry PERSON1)) (not (thirsty PERSON1)) (not (injured PERSON1))
            (not (hungry PERSON2)) (not (thirsty PERSON2)) (not (injured PERSON2))
            (not (hungry PERSON3)) (not (thirsty PERSON3)) (not (injured PERSON3))
            
        )
    )
)