
(define (problem emergency-30-15)
    (:domain EMERGENCY)

    (:objects
        DEPOT UAV1 UAV2
        
            FOOD0 FOOD1 FOOD2 FOOD3 FOOD4 
        
            WATER0 WATER1 WATER2 WATER3 WATER4 
        
            DRUGS0 DRUGS1 DRUGS2 DRUGS3 DRUGS4 
        
            PERSON0 PERSON1 PERSON2 PERSON3 PERSON4 PERSON5 PERSON6 PERSON7 PERSON8 PERSON9 PERSON10 PERSON11 PERSON12 PERSON13 PERSON14 PERSON15 PERSON16 PERSON17 PERSON18 PERSON19 PERSON20 PERSON21 PERSON22 PERSON23 PERSON24 PERSON25 PERSON26 PERSON27 PERSON28 PERSON29 
        
    )

    (:init
        (location DEPOT) (depot DEPOT)
        (helicopter UAV1) (helicopter UAV2) (empty UAV1) (empty UAV2) (at UAV1 DEPOT) (at UAV2 DEPOT)

        (location PERSON0) (person PERSON0)
        (location PERSON1) (person PERSON1)
        (location PERSON2) (person PERSON2)
        (location PERSON3) (person PERSON3)
        (location PERSON4) (person PERSON4)
        (location PERSON5) (person PERSON5)
        (location PERSON6) (person PERSON6)
        (location PERSON7) (person PERSON7)
        (location PERSON8) (person PERSON8)
        (location PERSON9) (person PERSON9)
        (location PERSON10) (person PERSON10)
        (location PERSON11) (person PERSON11)
        (location PERSON12) (person PERSON12)
        (location PERSON13) (person PERSON13)
        (location PERSON14) (person PERSON14)
        (location PERSON15) (person PERSON15)
        (location PERSON16) (person PERSON16)
        (location PERSON17) (person PERSON17)
        (location PERSON18) (person PERSON18)
        (location PERSON19) (person PERSON19)
        (location PERSON20) (person PERSON20)
        (location PERSON21) (person PERSON21)
        (location PERSON22) (person PERSON22)
        (location PERSON23) (person PERSON23)
        (location PERSON24) (person PERSON24)
        (location PERSON25) (person PERSON25)
        (location PERSON26) (person PERSON26)
        (location PERSON27) (person PERSON27)
        (location PERSON28) (person PERSON28)
        (location PERSON29) (person PERSON29)
        

        (crate FOOD0) (food_crate FOOD0) (at FOOD0 DEPOT)
        (crate FOOD1) (food_crate FOOD1) (at FOOD1 DEPOT)
        (crate FOOD2) (food_crate FOOD2) (at FOOD2 DEPOT)
        (crate FOOD3) (food_crate FOOD3) (at FOOD3 DEPOT)
        (crate FOOD4) (food_crate FOOD4) (at FOOD4 DEPOT)
        

        (crate WATER0) (water_crate WATER0) (at WATER0 DEPOT)
        (crate WATER1) (water_crate WATER1) (at WATER1 DEPOT)
        (crate WATER2) (water_crate WATER2) (at WATER2 DEPOT)
        (crate WATER3) (water_crate WATER3) (at WATER3 DEPOT)
        (crate WATER4) (water_crate WATER4) (at WATER4 DEPOT)
        

        (crate DRUGS0) (drugs_crate DRUGS0) (at DRUGS0 DEPOT)
        (crate DRUGS1) (drugs_crate DRUGS1) (at DRUGS1 DEPOT)
        (crate DRUGS2) (drugs_crate DRUGS2) (at DRUGS2 DEPOT)
        (crate DRUGS3) (drugs_crate DRUGS3) (at DRUGS3 DEPOT)
        (crate DRUGS4) (drugs_crate DRUGS4) (at DRUGS4 DEPOT)
        

        (hungry PERSON25) (hungry PERSON0) (hungry PERSON19) (hungry PERSON26) (hungry PERSON16) 
        (thirsty PERSON28) (thirsty PERSON25) (thirsty PERSON11) (thirsty PERSON4) (thirsty PERSON0) 
        (injured PERSON18) (injured PERSON0) (injured PERSON2) (injured PERSON8) (injured PERSON24) 
    )

    (:goal
        (and
            (not (hungry PERSON25)) (not (hungry PERSON0)) (not (hungry PERSON19)) (not (hungry PERSON26)) (not (hungry PERSON16)) 
            (not (thirsty PERSON28)) (not (thirsty PERSON25)) (not (thirsty PERSON11)) (not (thirsty PERSON4)) (not (thirsty PERSON0)) 
            (not (injured PERSON18)) (not (injured PERSON0)) (not (injured PERSON2)) (not (injured PERSON8)) (not (injured PERSON24)) 
        )
    )
)