
(define (problem emergency-40-30)
    (:domain EMERGENCY)

    (:objects
        DEPOT UAV1 UAV2
        
            FOOD0 FOOD1 FOOD2 FOOD3 FOOD4 FOOD5 FOOD6 FOOD7 FOOD8 FOOD9 
        
            WATER0 WATER1 WATER2 WATER3 WATER4 WATER5 WATER6 WATER7 WATER8 WATER9 
        
            DRUGS0 DRUGS1 DRUGS2 DRUGS3 DRUGS4 DRUGS5 DRUGS6 DRUGS7 DRUGS8 DRUGS9 
        
            PERSON0 PERSON1 PERSON2 PERSON3 PERSON4 PERSON5 PERSON6 PERSON7 PERSON8 PERSON9 PERSON10 PERSON11 PERSON12 PERSON13 PERSON14 PERSON15 PERSON16 PERSON17 PERSON18 PERSON19 PERSON20 PERSON21 PERSON22 PERSON23 PERSON24 PERSON25 PERSON26 PERSON27 PERSON28 PERSON29 PERSON30 PERSON31 PERSON32 PERSON33 PERSON34 PERSON35 PERSON36 PERSON37 PERSON38 PERSON39 
        
    )

    (:init
        (location DEPOT) (depot DEPOT)
        (helicopter UAV1) (helicopter UAV2) (empty UAV1) (empty UAV2) (at UAV1 DEPOT) (at UAV2 DEPOT)

        (location PERSON0) (person PERSON0)
        (location PERSON1) (person PERSON1)
        (location PERSON2) (person PERSON2)
        (location PERSON3) (person PERSON3)
        (location PERSON4) (person PERSON4)
        (location PERSON5) (person PERSON5)
        (location PERSON6) (person PERSON6)
        (location PERSON7) (person PERSON7)
        (location PERSON8) (person PERSON8)
        (location PERSON9) (person PERSON9)
        (location PERSON10) (person PERSON10)
        (location PERSON11) (person PERSON11)
        (location PERSON12) (person PERSON12)
        (location PERSON13) (person PERSON13)
        (location PERSON14) (person PERSON14)
        (location PERSON15) (person PERSON15)
        (location PERSON16) (person PERSON16)
        (location PERSON17) (person PERSON17)
        (location PERSON18) (person PERSON18)
        (location PERSON19) (person PERSON19)
        (location PERSON20) (person PERSON20)
        (location PERSON21) (person PERSON21)
        (location PERSON22) (person PERSON22)
        (location PERSON23) (person PERSON23)
        (location PERSON24) (person PERSON24)
        (location PERSON25) (person PERSON25)
        (location PERSON26) (person PERSON26)
        (location PERSON27) (person PERSON27)
        (location PERSON28) (person PERSON28)
        (location PERSON29) (person PERSON29)
        (location PERSON30) (person PERSON30)
        (location PERSON31) (person PERSON31)
        (location PERSON32) (person PERSON32)
        (location PERSON33) (person PERSON33)
        (location PERSON34) (person PERSON34)
        (location PERSON35) (person PERSON35)
        (location PERSON36) (person PERSON36)
        (location PERSON37) (person PERSON37)
        (location PERSON38) (person PERSON38)
        (location PERSON39) (person PERSON39)
        

        (crate FOOD0) (food_crate FOOD0) (at FOOD0 DEPOT)
        (crate FOOD1) (food_crate FOOD1) (at FOOD1 DEPOT)
        (crate FOOD2) (food_crate FOOD2) (at FOOD2 DEPOT)
        (crate FOOD3) (food_crate FOOD3) (at FOOD3 DEPOT)
        (crate FOOD4) (food_crate FOOD4) (at FOOD4 DEPOT)
        (crate FOOD5) (food_crate FOOD5) (at FOOD5 DEPOT)
        (crate FOOD6) (food_crate FOOD6) (at FOOD6 DEPOT)
        (crate FOOD7) (food_crate FOOD7) (at FOOD7 DEPOT)
        (crate FOOD8) (food_crate FOOD8) (at FOOD8 DEPOT)
        (crate FOOD9) (food_crate FOOD9) (at FOOD9 DEPOT)
        

        (crate WATER0) (water_crate WATER0) (at WATER0 DEPOT)
        (crate WATER1) (water_crate WATER1) (at WATER1 DEPOT)
        (crate WATER2) (water_crate WATER2) (at WATER2 DEPOT)
        (crate WATER3) (water_crate WATER3) (at WATER3 DEPOT)
        (crate WATER4) (water_crate WATER4) (at WATER4 DEPOT)
        (crate WATER5) (water_crate WATER5) (at WATER5 DEPOT)
        (crate WATER6) (water_crate WATER6) (at WATER6 DEPOT)
        (crate WATER7) (water_crate WATER7) (at WATER7 DEPOT)
        (crate WATER8) (water_crate WATER8) (at WATER8 DEPOT)
        (crate WATER9) (water_crate WATER9) (at WATER9 DEPOT)
        

        (crate DRUGS0) (drugs_crate DRUGS0) (at DRUGS0 DEPOT)
        (crate DRUGS1) (drugs_crate DRUGS1) (at DRUGS1 DEPOT)
        (crate DRUGS2) (drugs_crate DRUGS2) (at DRUGS2 DEPOT)
        (crate DRUGS3) (drugs_crate DRUGS3) (at DRUGS3 DEPOT)
        (crate DRUGS4) (drugs_crate DRUGS4) (at DRUGS4 DEPOT)
        (crate DRUGS5) (drugs_crate DRUGS5) (at DRUGS5 DEPOT)
        (crate DRUGS6) (drugs_crate DRUGS6) (at DRUGS6 DEPOT)
        (crate DRUGS7) (drugs_crate DRUGS7) (at DRUGS7 DEPOT)
        (crate DRUGS8) (drugs_crate DRUGS8) (at DRUGS8 DEPOT)
        (crate DRUGS9) (drugs_crate DRUGS9) (at DRUGS9 DEPOT)
        

        (hungry PERSON23) (hungry PERSON37) (hungry PERSON29) (hungry PERSON19) (hungry PERSON21) (hungry PERSON3) (hungry PERSON31) (hungry PERSON20) (hungry PERSON38) (hungry PERSON16) 
        (thirsty PERSON16) (thirsty PERSON26) (thirsty PERSON35) (thirsty PERSON32) (thirsty PERSON20) (thirsty PERSON12) (thirsty PERSON15) (thirsty PERSON33) (thirsty PERSON10) (thirsty PERSON25) 
        (injured PERSON15) (injured PERSON20) (injured PERSON17) (injured PERSON37) (injured PERSON31) (injured PERSON16) (injured PERSON30) (injured PERSON36) (injured PERSON8) (injured PERSON0) 
    )

    (:goal
        (and
            (not (hungry PERSON23)) (not (hungry PERSON37)) (not (hungry PERSON29)) (not (hungry PERSON19)) (not (hungry PERSON21)) (not (hungry PERSON3)) (not (hungry PERSON31)) (not (hungry PERSON20)) (not (hungry PERSON38)) (not (hungry PERSON16)) 
            (not (thirsty PERSON16)) (not (thirsty PERSON26)) (not (thirsty PERSON35)) (not (thirsty PERSON32)) (not (thirsty PERSON20)) (not (thirsty PERSON12)) (not (thirsty PERSON15)) (not (thirsty PERSON33)) (not (thirsty PERSON10)) (not (thirsty PERSON25)) 
            (not (injured PERSON15)) (not (injured PERSON20)) (not (injured PERSON17)) (not (injured PERSON37)) (not (injured PERSON31)) (not (injured PERSON16)) (not (injured PERSON30)) (not (injured PERSON36)) (not (injured PERSON8)) (not (injured PERSON0)) 
        )
    )
)